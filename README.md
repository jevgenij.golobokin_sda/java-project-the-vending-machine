# Vending Machine app
(in progress)

### Requirements

Implement a traditional vending machine which:

1. Allow user to select products - the menu is displayed in console.
2. Allow user to select what coins to insert - menu is displayed in console.
3. Allow user to take refund by canceling the request.
4. Return selected product and remaining change if any.
5. The state of the vending machine is saved in a file on the disk - a json file.
6. Vending Machine has the product menu configurable - new products can be added in the json file.
7. Vending Machine is configurable on what coins it accepts - new coins can be added in the json file.

### How to run
1. This is a Maven project with console UI
2. Clone or download project
3. Run Main.java

### Keywords
<i>OOP, Encapsulation, Composition, Interfaces, Collections, Exceptions, Lambda, Streams, Gson, json, DTO
</i>

