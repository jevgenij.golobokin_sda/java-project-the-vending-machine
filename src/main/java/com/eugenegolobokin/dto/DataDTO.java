package com.eugenegolobokin.dto;

import com.eugenegolobokin.models.Coin;
import com.eugenegolobokin.models.Product;

import java.util.List;

public class DataDTO {
    private List<Product> products;
    private List<Coin> wallet;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Coin> getWallet() {
        return wallet;
    }

    public void setWallet(List<Coin> wallet) {
        this.wallet = wallet;
    }

}
