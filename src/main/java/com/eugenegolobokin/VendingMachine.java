package com.eugenegolobokin;

import com.eugenegolobokin.models.Product;
import com.eugenegolobokin.services.ConsoleGraphicalService;
import com.eugenegolobokin.services.DatabaseService;
import com.eugenegolobokin.services.UserInterfaceService;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class VendingMachine {

    public DatabaseService dbService;
    public UserInterfaceService uiService;
    public ConsoleGraphicalService graphicalService;
    private Map<Product, Integer> productStock = new HashMap<>();
    private Map<Integer, List<Product>> productsByCode = new HashMap<>();
    public boolean isRunning = true;
    private Wallet wallet;
    private Product selectedProduct = null;
    private List<Integer> change = new ArrayList<>();


    public VendingMachine(DatabaseService dbService, UserInterfaceService uiService) {
        this.dbService = dbService;
        this.uiService = uiService;
        this.graphicalService = new ConsoleGraphicalService();
        loadProducts();
        this.wallet = new Wallet(dbService);
    }

    private void loadProducts() {
        productStock = dbService.getProductList()
                .stream()
                .collect(Collectors.toMap(product -> product, Product::getNumberInStock));

        mapProductsByCode();
    }

    private void mapProductsByCode() {
        productsByCode = dbService.getProductList()
                .stream()
                .collect(groupingBy(Product::getCode));
    }

    public void run() {

        uiService.displayMessage("%s", "\n---------- WELCOME TO THE VENDING MACHINE!! ----------\n");

        while (isRunning) {

            uiService.displayProductMenu(productStock);
            selectProduct();

            uiService.displayWallet(wallet);
            requestPayment();

            approvePayment();


        }
    }

    private void requestPayment() {

        int sumOfInsertedCoins = 0;
        int priceToPay = (int) (selectedProduct.getPrice() * 100);

        while (sumOfInsertedCoins < priceToPay) {
            uiService.displayMessage("Left to pay: %dct", priceToPay - sumOfInsertedCoins);
            int insertedCoin = uiService.enterCoin();

            if (wallet.getWallet().containsKey(insertedCoin) && wallet.getWallet().get(insertedCoin) > 0) {
                sumOfInsertedCoins += insertedCoin;
                wallet.updateTempWallet((insertedCoin * -1));
            } else {
                System.out.println("There are no such coins in your wallet!\n");
            }
        }

        uiService.displayMessage("Total coins inserted: %dct", sumOfInsertedCoins);

        calculateChange(sumOfInsertedCoins, priceToPay);

    }

    private void approvePayment() {
        if (uiService.question("Do you want to buy selected item: %s, %s?",
                selectedProduct.getName(), selectedProduct.getSize())) {
            completeOrder();
        } else {
            cancelOrder();
        }
    }

    private void completeOrder() {
        uiService.displayMessage("Order completed! Please take your item: %s",
                selectedProduct.getName());
        graphicalService.printProduct(selectedProduct.getCode());

        updateLocalProductStock();

        if (change.size() > 0) {
            showChangeCoins(change);
        } ;

        dbService.saveData(transferProductsToDB(), wallet.transferWalletToDB());

        isRunning = false;
    }

    private void cancelOrder() {
        selectedProduct = null;
        wallet.resetTempWallet();
    }

    private void selectProduct() {
        int code = uiService.enterProductCode();
        selectedProduct = null;

        if (!productsByCode.containsKey(code)) {
            uiService.displayMessage("Sorry, but product with code %d doesn't exist", code);
            selectProduct();
        } else if (productsByCode.get(code).get(0).getNumberInStock() <= 0) {
            uiService.displayMessage("Sorry, this product is out of stock!");
            selectProduct();
        } else {
            selectedProduct = productsByCode.get(code).get(0);
            uiService.displayMessage("Your selection: %s, %s", selectedProduct.getName(), selectedProduct.getSize());
            double selectedProductPrice = selectedProduct.getPrice();
            uiService.displayMessage("Price to pay: %.2f EUR\n", selectedProductPrice);
        }

    }

    private void updateLocalProductStock() {
        selectedProduct.setNumberInStock(selectedProduct.getNumberInStock() - 1);
        productStock.replace(selectedProduct, selectedProduct.getNumberInStock());
    }

    private List<Product> transferProductsToDB() {
        return productStock
                .entrySet()
                .stream()
                .map(c -> {
                    String name = c.getKey().getName();
                    String size = c.getKey().getSize();
                    double price = c.getKey().getPrice();
                    int code = c.getKey().getCode();
                    int numberInStock = c.getKey().getNumberInStock();
                    return Product.transform(name, size, price, code, numberInStock);
                })
                .sorted(Comparator.comparingInt(p -> p.getCode()))
                .collect(Collectors.toList());
    }

    private void calculateChange(int sumOfInsertedCoins, int priceToPay) {
        if (sumOfInsertedCoins - priceToPay == 0) {
            return;
        } else {
            int leftToRefund = sumOfInsertedCoins - priceToPay;
            List<Integer> coinOptions = Arrays.asList(50, 20, 10);
            while (leftToRefund > 0) {
                for (Integer coin : coinOptions) {
                    if (coin > leftToRefund) {
                        continue;
                    }
                    change.add(coin);
                    wallet.updateTempWallet(coin);
                    leftToRefund -= coin;
                }
            }
        }
    }

    private void showChangeCoins(List<Integer> change) {
        System.out.print("Your change: ");
        for (Integer integer : change) {
            System.out.print(integer + "ct ");
        }
    }

}
