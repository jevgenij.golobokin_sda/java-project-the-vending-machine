package com.eugenegolobokin;

import com.eugenegolobokin.models.Coin;
import com.eugenegolobokin.services.DatabaseService;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Wallet {
    public DatabaseService dbService;
    private Map<Integer, Integer> wallet = new HashMap<>();
    private Map<Integer, Integer> tempWallet;

    public Wallet(DatabaseService dbService) {
        this.dbService = dbService;
        loadWallet(dbService);
        resetTempWallet();
    }

    private void loadWallet(DatabaseService dbService) {
        this.wallet = dbService.getWallet()
                .stream()
                .collect(Collectors.toMap(Coin::getCoin, Coin::getCount));
    }

    public double countMoney() {
        double totalSumOfCents = wallet.entrySet()
                .stream()
                .mapToDouble(c -> c.getKey() * c.getValue()).sum();
        return totalSumOfCents / 100;
    }

    public Map<Integer, Integer> getWallet() {
        return wallet;
    }

    public void updateTempWallet(int coin) {
        if (coin < 0) {
            coin *= -1;
            tempWallet.replace(coin, tempWallet.get(coin), tempWallet.get(coin) - 1);
        } else {
            tempWallet.replace(coin, tempWallet.get(coin), tempWallet.get(coin) + 1);
        }
    }

    public void resetTempWallet() {
        this.tempWallet = this.wallet;
    }

    public List<Coin> transferWalletToDB() {
        return tempWallet
                .entrySet()
                .stream()
                .sorted(Comparator.comparingInt(c -> c.getKey()))
                .map(c -> {
                    int coin = c.getKey();
                    int count = c.getValue();
                    return Coin.transform(coin, count);
                })
                .collect(Collectors.toList());
    }
}
