package com.eugenegolobokin;

import com.eugenegolobokin.services.ConsoleUserInterfaceService;
import com.eugenegolobokin.services.DatabaseService;
import com.eugenegolobokin.services.JsonDatabaseService;
import com.eugenegolobokin.services.UserInterfaceService;

public class Main {
    public static void main(String[] args) {

        DatabaseService dbService = new JsonDatabaseService();
        UserInterfaceService uiService = new ConsoleUserInterfaceService();

        VendingMachine vendingMachine = new VendingMachine(dbService, uiService);
        vendingMachine.run();


    }
}
