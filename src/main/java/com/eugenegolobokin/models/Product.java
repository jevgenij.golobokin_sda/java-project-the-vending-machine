package com.eugenegolobokin.models;

public class Product {
    private final String name;
    private final String size;
    private final double price;
    private final int code;
    private int numberInStock;

    public Product(String name, String size, double price, int code, int numberInStock) {
        this.name = name;
        this.size = size;
        this.price = price;
        this.code = code;
        this.numberInStock = numberInStock;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }

    public int getCode() {
        return code;
    }

    public int getNumberInStock() {
        return numberInStock;
    }

    public void setNumberInStock(int numberInStock) {
        this.numberInStock = numberInStock;
    }

    @Override
    public String toString() {
        return String.format("<Product code: %d name: %s size: %s price: %.2f>", code, name, size, price);
    }

    public static Product transform(String name, String size, double price, int code, int numberInStock) {
        return new Product(name, size, price, code, numberInStock);
    }

}
