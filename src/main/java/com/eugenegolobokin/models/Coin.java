package com.eugenegolobokin.models;

public class Coin {
    private int coin;
    private int count;

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String printCoin() {
        String coinName;
        switch (coin) {
            case 10:
            case 20:
            case 50:
                coinName = "ct";
                break;
            case 1:
            case 2:
            case 5:
                coinName = "EUR";
                break;
            default:
                coinName = "";
        }
        return coin + coinName;
    }

    public static Coin transform(int coin, int count) {
        Coin coinDTO = new Coin();
        coinDTO.setCoin(coin);
        coinDTO.setCount(count);
        return coinDTO;
    }
}
