package com.eugenegolobokin.services;

import com.eugenegolobokin.dto.DataDTO;
import com.eugenegolobokin.models.Coin;
import com.eugenegolobokin.models.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JsonDatabaseService implements DatabaseService {

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final String DB_FILE_PATH = "src/main/resources/data.json";
    private DataDTO dataDTO = new DataDTO();

    public JsonDatabaseService() {
        readData();
    }

    public void readData() {

        try (FileReader fileReader = new FileReader(DB_FILE_PATH)) {
            this.dataDTO = gson.fromJson(fileReader, DataDTO.class);
        } catch (FileNotFoundException ex) {
            System.err.println("Failed to read file." + ex);
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }

    @Override
    public List<Product> getProductList() {
        return dataDTO.getProducts();
    }

    @Override
    public List<Coin> getWallet() {
        return dataDTO.getWallet();
    }

    @Override
    public void saveData(List<Product> products, List<Coin> coins) {
        dataDTO.setProducts(products);
        dataDTO.setWallet(coins);
        storeData(dataDTO);
    }

    private void storeData(DataDTO dataDTO) {
        try (FileWriter fileWriter = new FileWriter(DB_FILE_PATH)) {
            gson.toJson(dataDTO, fileWriter);
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }
}
