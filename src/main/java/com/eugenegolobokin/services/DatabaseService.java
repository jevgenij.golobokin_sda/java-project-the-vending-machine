package com.eugenegolobokin.services;

import com.eugenegolobokin.models.Coin;
import com.eugenegolobokin.models.Product;

import java.util.List;

public interface DatabaseService {

    List<Product> getProductList();
    List<Coin> getWallet();
    void saveData(List<Product> products, List<Coin> coins);

}
