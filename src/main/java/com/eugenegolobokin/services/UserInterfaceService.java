package com.eugenegolobokin.services;

import com.eugenegolobokin.Wallet;
import com.eugenegolobokin.models.Product;

import java.util.Map;

public interface UserInterfaceService {

    void displayMessage(String message, Object... args);
    int enterProductCode();
    int enterCoin();
    boolean question(String message, Object... args);
    void displayProductMenu(Map<Product, Integer> productStock);
    void displayWallet(Wallet wallet);

}