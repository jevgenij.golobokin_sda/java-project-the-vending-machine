package com.eugenegolobokin.services;

import com.eugenegolobokin.Wallet;
import com.eugenegolobokin.models.Product;

import java.util.*;

public class ConsoleUserInterfaceService implements UserInterfaceService {

    private Scanner scanner = new Scanner(System.in);

    @Override
    public void displayMessage(String message, Object... args) {
        System.out.printf(message + "\n", args);
    }

    @Override
    public int enterProductCode() {
        System.out.println("Please enter product code: ");
        if (!scanner.hasNextInt()) {
            scanner.next();
            System.err.println("Please enter only valid product code (i.e.: 2)");
            return enterProductCode();
        } else {
            return scanner.nextInt();
        }
    }

    @Override
    public int enterCoin() {
        System.out.println("Please insert coin: ");
        if (!scanner.hasNextInt()) {
            scanner.next();
            System.err.println("Please insert only valid coins (i.e.: 20)");
            return enterCoin();
        } else {
            return scanner.nextInt();
        }
    }

    @Override
    public boolean question(String message, Object... args) {
        displayMessage(message + " (Y/n)", args);
        String answer = scanner.next();
        return answer.toLowerCase().startsWith("y");

    }

    @Override
    public void displayProductMenu(Map<Product, Integer> productStock) {
        System.out.printf("%-5s %-25s %-7s %-7s %6s\n", "Code", "Product Name", "Size", "Price", "Stock");
        System.out.println("------------------------------------------------------");

        productStock.keySet()
                .stream()
                .sorted(Comparator.comparingInt(p -> p.getCode()))
                .forEach(p -> displayMessage("%-5d %-25s %-7s %-7.2f %6d",
                        p.getCode(), p.getName(), p.getSize(), p.getPrice(), p.getNumberInStock()));

        System.out.println("------------------------------------------------------\n");
        System.out.println("SELECT ITEM TO BUY");
    }

    @Override
    public void displayWallet(Wallet wallet) {
        displayMessage("You have total %.2f EUR in your wallet", wallet.countMoney());
        displayMessage("Coins in your wallet:");
        System.out.printf("%-7s %-7s\n", "Coin", "Count");
        System.out.println("--------------");

        wallet.getWallet().entrySet()
                .stream()
                .sorted(Comparator.comparingInt(c -> c.getKey()))
                .forEach(c -> displayMessage("%-7s %-7s", c.getKey(), c.getValue()));

        System.out.println("--------------");
        System.out.println("SELECT COINS TO INSERT");
    }


}
